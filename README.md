This repository contains the participant files for the ARCH Verification Competition: https://cps-vo.org/group/ARCH/FriendlyCompetition.

Repository Organization and Directory Structure
===

The repository is organized by the year of the competition, followed by categories, followed by specific tools participating in a given category. For example, in the affine category (AFF) for 2017, one may find the execution instructions and results for the HyDRA tool at 2017\AFF\hydra\ (https://gitlab.com/goranf/ARCH-COMP/tree/master/2017/AFF/hydra).

Sparse checkout for single year
===

To get a local copy for only a single year, you can use Git's sparse checkout.

The example below assumes the year 2019.
Here `<url-of-your-fork>` is the URL of this repository (i.e., https://gitlab.com/goranf/ARCH-COMP.git) respectively of your fork.

```bash
mkdir ARCH-COMP-2019
cd ARCH-COMP-2019
git init
git remote add -f origin <url-of-your-fork>
git config core.sparseCheckout true
echo "2019/" >> .git/info/sparse-checkout
git pull origin master
```

Repeatability Evaluation Instructions (2020)
===

This year (2020), basically the same process as last year (2019) will be used (see below), which requires each participant submitting (1) a Dockerfile and (2) a batch execution script across all benchmarks.

The only addition is that for each category, we request a script that will execute all tools in the category, ideally producing together the results tables/figures appearing in the category report.

A basic representative script for doing this for the 2019 AFF category, which participants may want to build upon, can be found here:

https://gitlab.com/goranf/ARCH-COMP/-/blob/master/2019/AFF/docker_batch.py

For tools that require 3rd party licenses, particularly Matlab, one may consider CodeOcean: http://codeocean.com/

Repeatability Evaluation Instructions (2019)
===

This year (2019), we require participants to submit Dockerfiles for the repeatability evaluation.

The submission requirements for repeatability are, for each tool participating in each category:
1. A Dockerfile that sets up a container with all necessary libraries, tools, benchmarks, etc. to execute your tool. This ideally can pull all necessary requirements from online repositories (including this ARCH-COMP one), but if necessary, a separate archive containing the necessary files can be submitted along with the Dockerfile in this repository. If you refer to repositories in your Dockerfile (e.g., through wget, git pulls, etc.), you MUST include version/commit numbers or tag names, as otherwise the repeatability may fail in the future if the repository structure changes (e.g., if files are moved in later versions of the repositories you're including files from).
2. A script (Shell, Python, etc.) that executes all benchmarks analyzed in your tool, ideally with some clear results indicating the status of each benchmark (table, logs, reach set images, etc.).

The submission of the repeatability evaluation files to this repository should follow the forking repository (pull/merge requests) as in the 2018 instructions below.

If a tool participates in multiple categories, we expect the Dockerfiles, execution scripts, and benchmarks to be clearly indicated for these. Specifically, a single Dockerfile to set up the container is sufficient if all variants of the tool rely on the same set of libraries, etc., but execution scripts and benchmarks should be tailored and submitted for the relevant category and put in the appropriate category folder in the repository.

If you're not familiar with Docker, it essentially provides lightweight virtual machines. If you want to get some familiarity with Docker, these are reasonable tutorials, and also attached is an example Dockerfile that installs dReach/dReal for illustration:

dReach/dReal Installation Dockerfile: https://gitlab.com/goranf/ARCH-COMP/blob/master/Dockerfile.dreach.build-example

Docker Tutorial: https://github.com/docker/labs/blob/master/beginner/chapters/alpine.md

Example Dockerfile to set up a custom python application:
https://hub.docker.com/_/python and
https://runnable.com/docker/python/dockerize-your-python-application

Doing things in this way will streamline the repeatability and make installation/setup/execution of tools consistent versus everyone submitting custom readmes or scripts to install/run tools, etc., all of which may still be done, but will be set up appropriately through commands in the Dockerfiles.

In conjunction with NSF, we have been working on a framework within the CPS-VO to provide mechanisms for competitions such as this, with the initial goal to aid repeatability (some details here: https://cps-vo.org/group/verification_tools/ ). As a part of this, we are planning to try a cloud-based infrastructure to execute the participating tools for repeatability. In the future, this may enable some performance comparisons beyond what we've done so far for repeatability, which is just to install/run the tools and check for comparable verification results on the benchmarks. If a Dockerfile is provided for a tool and if there is interest from the participants, then it will be easy to be able to execute the tool via the CPS-VO in a browser. We will add further details on this shortly, if you are interested to play with the existing prototype, you can request to join the group here: https://cps-vo.org/group/hyst


Repeatability Evaluation Instructions (2018)
===

To ease repeatability and facilitate the exchange of benchmarks for the competition, participants should submit their repeatability evaluation packages as pull requests (merge requests in GitLab lingo) to this repository.

More details on a forking repository management workflow are here: https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow

More details on GitLab's merge requests versus GitHub's pull requests, they're in essence the same thing: https://docs.gitlab.com/ee/workflow/gitlab_flow.html#merge-pull-requests-with-gitlab-flow

The basic process to submit your repeatability evaluation benchmarks and executables is:

1. Fork this repository to your own gitlab account: https://gitlab.com/goranf/ARCH-COMP.git
2. Commit changes to your fork (if you already made changes to the main repository line, just copy/paste the source files in the new fork and commit them). You should place all necessary benchmarks, readmes, tools, installation/execution scripts, etc. to reproduce your results in each category in which you have participated (e.g., say you participated in AFF and NLN with a tool called HTEST, then you'd add your files to 2018/AFF/htest/ and 2018/NLN/htest/). If it is possible to easily and clearly designate your input benchmarks and results in the directory structure, please do so (e.g., continuing the example, place the benchmark files at 2018/AFF/htest/benchmark/ and the results/output files, logs, etc. in 2018/AFF/htest/result/ ).
3. Push changes to your fork
4. Issue pull request (merge request in GitLab lingo), we will review it and then approve if it looks good

As there may be some concerns regarding large files (e.g., if you're using a virtual machine for your repeatability package), licenses for your tools or dependencies, compiling your tool if it has external libraries, etc., please contact the evaluation chair Taylor Johnson with questions: http://www.taylortjohnson.com/?m=contact

Eventually, we hope this process will facilitate being able to clone this repository, then executing a single script to install all tools perhaps on Docker containers or AWS virtual machines shared so that participants may access them to test, then another to execute everything against all benchmarks to reproduce the competition results.