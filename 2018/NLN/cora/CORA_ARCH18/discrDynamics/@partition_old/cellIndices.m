function [cellIndices]=cellIndices(Obj,subscriptVectors)
% DEPRECATED -- FUNCTIONALITY PROVIDED BY segmentIndices.m
% Purpose:  accesses private s2i function
% Pre:      partition object, subscript vector
% Post:     cell indices
% Built:    17.08.07,MA


[cellIndices]=s2i(Obj,subscriptVectors);