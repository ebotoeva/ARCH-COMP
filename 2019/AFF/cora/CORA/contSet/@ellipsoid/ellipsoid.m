classdef ellipsoid
% ellipsoid - Object and Copy Constructor 
% Some ideas for this class have been extracted from 
%Kurzhanskiy, A.A. and Varaiya, P., 2006, December. Ellipsoidal toolbox (ET). In Proceedings of the 45th IEEE Conference on Decision and Control (pp. 1498-1503). IEEE.
%
% Syntax:  
%    object constructor: Obj = ellipsoids(varargin)
%    copy constructor: Obj = otherObj
%
%(Overloaded) Methods:
% - ellipsoid(E): E-ellipsoids object
% - ellipsoid(Q): Q positive semidefinite, symmetric matrix
% - ellipsoid(Q,q): Q, q center of ellipsoid
% - ellipsoid(Q,q,TOL): Q,q, TOL is tolerance for psd check
%
% Outputs:
%    Obj - Generated Object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      14-September-2006 
% Last update:  22-March-2007
%               04-June-2010
%               08-February-2011
%               18-November-2015
%               05-December-2017 (DG) class is redefined in complience with
%               the new standard.
% Last revision: ---

%------------- BEGIN CODE --------------

properties (SetAccess = protected, GetAccess = public)
    Q=[];
    q=[];
    TOL=[];
    dim=[];
    isdegenerate=[];
end
   
methods

    function Obj = ellipsoid(varargin)
        Obj.TOL = 1e-11;
        Obj.isdegenerate = false;
        if nargin==3
            Obj.TOL = varargin{3};
        end
        
        %Check psd and symmetry of Q
        if nargin>=1
            mev = min(eig(varargin{1}));
            if ~ellipsoid.issymmetric(varargin{1},Obj.TOL) || (mev<0 && abs(mev)>Obj.TOL)
                error('Q needs to be positive semidefinite/symmetric');
            end
        end
            
        % If 1 argument is passed
        if nargin == 1
            %is input a ellipsoid?
            if isa(varargin{1},'ellipsoid')
                Obj = varargin{1};
            else
                %List elements of the class
                Obj.Q=varargin{1}; 
                Obj.q=zeros(size(Obj.Q,1),1); 
            end


        % If 2 arguments are passed
        elseif nargin >= 2
            %List elements of the class
            Obj.Q=varargin{1};
            if length(Obj.Q)~=length(varargin{2})
                error('Q and q dimensions are not matching');
            end
            Obj.q=varargin{2};
            
        % Else if not enough or too many inputs are passed    
        else
            error('This class needs more/less input values');
            %Obj=[];
        end
        %Determine the rank of Q; helpful to e.g. determine whether
        %ellipsoid is degenerate or not
        Obj.dim=rank(Obj.Q,Obj.TOL);
        if Obj.dim<length(Obj.Q)
            Obj.isdegenerate=true;
        end
    end
end
methods (Static=true)
    [E] = generate(varargin)
    res = issymmetric(varargin)
end
end
%------------- END OF CODE --------------