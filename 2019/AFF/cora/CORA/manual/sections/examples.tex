This section presents a variety of examples that have been published in different papers. For each example, we provide a reference to the paper so that the details of the system can be studied there. The focus of this manual is on how the examples in the papers can be realized using CORA---this, of course, is not shown in scientific papers due to space restrictions. The examples are categorized along the different classes for dynamic systems realized in CORA. 

All subsequent examples can handle uncertain inputs. Uncertain parameters can be realized using different techniques:
\begin{enumerate}
 \item Introduce constant parameters as additional states and assign the dynamics $\dot{x}_i = 0$ to them. The disadvantage is that the dimension of the system is growing.
 \item Introduce time-varying parameters as additional uncertain inputs.
 \item Use specialized functions in CORA that can handle uncertain parameters.
\end{enumerate}
It is generally advised to use the last technique, but there is no proof that this technique always provides better results compared to the other techniques.


\subsection{Continuous Dynamics}

\subsubsection{Linear Dynamics} 

For linear dynamics, a simple academic example from \cite[Sec.~3.2.3]{Althoff2010a} is used with not much focus on a connection to a real system. However, since linear systems are solely determined by their state and input matrix, adjusting this example to any other linear system is straightforward. Here, the system dynamics is
\begin{equation*}
\dot{x}=\begin{bmatrix} 
-1 & -4 & 0 & 0 & 0 \\ 
4 & -1 & 0 & 0 & 0 \\
0 & 0 & -3 & 1 & 0 \\
0 & 0 & -1 & -3 & 0 \\
0 & 0 & 0 & 0 & -2 \\
\end{bmatrix}  x + u(t), \quad 
x(0)\in\begin{bmatrix} [0.9,1.1] \\ [0.9,1.1] \\ [0.9,1.1] \\ [0.9,1.1] \\ [0.9,1.1] \end{bmatrix}, \,
u(t) \in
\begin{bmatrix} 
[0.9,1.1] \\ 
[-0.25,0.25] \\
[-0.1,0.1] \\
[0.25,0.75] \\
[-0.75,-0.25] \\
\end{bmatrix}.
\end{equation*} 

The MATLAB code that implements the simulation and reachability analysis of the linear example is (the function is modified from the original file to better fit in this manual; line numbers after the first line jump due to the removed function description):
{\small
\input{./MATLABcode/example_linear_reach_01_5dim.tex}}

The reachable set and the simulation are plotted in Fig. \ref{fig:example_linear_reach_01_5dim} for a time horizon of $t_f = 5$.

\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_linear_reach_01_5dim_dim12.eps}
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_linear_reach_01_5dim_dim34.eps}
    \caption{Illustration of the reachable set of the linear example. The white box shows the initial set and the black lines show simulated trajectories.}
    \label{fig:example_linear_reach_01_5dim}		
\end{figure}


\subsubsection{Linear Dynamics with Uncertain Parameters}

For linear dynamics with uncertain parameters, we use the transmission line example from \cite[Sec.~4.5.2]{Althoff2011b}, which can be modeled as an electric circuit with resistors, inductors, and capacitors. The parameters of each component have uncertain values as described in \cite[Sec.~4.5.2]{Althoff2011b}. This example shows how one can better take care of dependencies of parameters by using matrix zonotopes instead of interval matrices. 

The MATLAB code that implements the simulation and reachability analysis of the linear example with uncertain parameters is (the function is modified from the original file to better fit in this manual; line numbers after the first line jump due to the removed function description):
{\small
\input{./MATLABcode/example_linearParam_reach_01_rlc_const.tex}}

The reachable set and the simulation are plotted in Fig.~\ref{fig:example_linearParam_reach_01_rlc_const} for a time horizon of $t_f = 0.7$. The plot showing the reachable set of the state $x_{20}$ over time is shown in Fig.~\ref{fig:example_linearParam_reach_01_rlc_const_time}.
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_linearParam_reach_01_rlc_const_dim_1_21.eps}
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_linearParam_reach_01_rlc_const_dim_20_40.eps}
    \caption{Illustration of the reachable set of the transmission example. The light gray shows the reachable set using matrix zonotopes and the dark gray shows the results using interval matrices. A white box shows the initial set and the black lines are simulated trajectories.}
    \label{fig:example_linearParam_reach_01_rlc_const}		
\end{figure}
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_linearParam_reach_01_rlc_const_time.eps}
    \caption{Illustration of the reachable set of the transmission example over time. The light gray shows the reachable set using matrix zonotopes and the dark gray shows the results using interval matrices. Black lines show simulated trajectories.}
    \label{fig:example_linearParam_reach_01_rlc_const_time}		
\end{figure}

\subsubsection{Nonlinear Dynamics}

For nonlinear dynamics, several examples are presented.

\paragraph{Tank System} 
The first example is the tank system from \cite{Althoff2008c} where water flows from one tank into another one. This example can be used to study the effect of water power plants on the water level of rivers. This example can be easy extended by several tanks and thus is a nice benchmark example to study the scalability of algorithms for reachability analysis. CORA can compute the reachable set with at least $100$ tanks.

The MATLAB code that implements the simulation and reachability analysis of the tank example is (the function is modified from the original file to better fit in this manual; line numbers after the first line jump due to the removed function description):
{\small
\input{./MATLABcode/example_nonlinear_reach_01_tank.tex}}

The difference to specifying a linear system is that a link to a nonlinear differential equation has to be provided, rather than the system matrix $A$ and the input matrix $B$. The nonlinear system model $\dot{x}=f(x,u)$, where $x$ is the state and $u$ is the input, is shown below:
{\small
\input{./MATLABcode/tank6Eq.tex}}

The output of this function is $\dot{x}$ for a given time $t$, state $x$, and input $u$. 

Fig. \ref{fig:example_nonlinear_reach_01_tank} shows the reachable set and the simulation for a time horizon of $t_f = 0.7$.
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinear_reach_01_tank_dim12_26Aug.eps} \hspace{-0.2cm}
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinear_reach_01_tank_dim34_26Aug.eps} \hspace{-0.2cm}
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinear_reach_01_tank_dim56_26Aug.eps}
    \caption{Illustration of the reachable set of the linear example. The white box shows the initial set and the black lines show simulated trajectories.}
    \label{fig:example_nonlinear_reach_01_tank}		
\end{figure}

\paragraph{Van der Pol Oscillator} 
The Van der Pol oscillator is a standard example for limit cycles. By using reachability analysis one can show that one always returns to the initial set so that the obtained set is an invariant set. This example is used in \cite{Althoff2008c} to demonstrate that one can obtain a solution even if the linearization error becomes too large by splitting the reachable set. Later, in \cite{Althoff2013a} an improved method is presented that requires less splitting. This example demonstrates the capabilities of the simpler approach presented in \cite{Althoff2008c}. Due to the similarity of the MATLAB code compared to the previous tank example, we only present the reachable set in Fig.~\ref{fig:example_nonlinear_reach_03_vanderPol}.
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.5\columnwidth]{./figures/examples/example_nonlinear_reach_03_vanderPol.eps} 
    \caption{Illustration of the reachable set of the Van der Pol oscillator. The white box shows the initial set and the black lines show simulated trajectories.}
    \label{fig:example_nonlinear_reach_03_vanderPol}		
\end{figure}

\paragraph{Seven-Dimensional Example for Non-Convex Set Representation} 
This academic example is used to demonstrate the benefits of using higher-order abstractions of nonlinear systems compared to linear abstractions. However, since higher order abstractions do not preserve convexity when propagating reachable sets, the non-convex set representation \textit{polynomial zonotope} is used as presented in \cite{Althoff2013a}. Please note that the entire reachable set for the complete time horizon is typically non-convex, even when the propagation from one point in time to another point in time is convex. Due to the similarity of the MATLAB code compared to the previous tank example, we only present the reachable set in Fig.~\ref{fig:example_nonlinear_reach_04_sevenDim_nonConvexRepr}.

\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinear_reach_04_sevenDim_nonConvexRepr_dim12.eps} \hspace{-0.2cm}
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinear_reach_04_sevenDim_nonConvexRepr_dim34.eps} \hspace{-0.2cm}
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinear_reach_04_sevenDim_nonConvexRepr_dim56.eps} 
    \caption{Illustration of the reachable set of the seven-dimensional example for non-convex set representation. The white box shows the initial set and the black lines show simulated trajectories.}
    \label{fig:example_nonlinear_reach_04_sevenDim_nonConvexRepr}		
\end{figure}

\paragraph{Autonomous Car Following a Reference Trajectory}
This example presents the reachable set of an automated vehicle developed at the German Aerospace Center. The difference of this example compared to the previous example is that a reference trajectory is followed. Similar models have been used in previous publications, see e.g., \cite{Althoff2011e,Althoff2012b,Althoff2014b}. In CORA, this only requires changing the input in \texttt{options.uTrans} from a vector to a matrix, where each column vector is the reference value at the next sampled point in time. Due to the similarity of the MATLAB code compared to the previous tank example, we only present the reachable set in Fig.~\ref{fig:example_nonlinear_reach_05_autonomousCar}, where the reference trajectory is plotted in red.
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_nonlinear_reach_05_autonomousCar_dim12_26Aug.eps} \hspace{-0.2cm}
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_nonlinear_reach_05_autonomousCar_dim34_26Aug.eps} 
    %\includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinear_reach_05_autonomousCar_dim56.eps} 
    \caption{Illustration of the reachable set of the seven-dimensional example for non-convex set representation. The white box shows the initial set and the black lines show simulated trajectories.}
    \label{fig:example_nonlinear_reach_05_autonomousCar}		
\end{figure}

\subsubsection{Nonlinear Dynamics with Uncertain Parameters}

As for linear systems, specialized algorithms have been developed for considering uncertain parameters of nonlinear systems. To better compare the results, we again use the tank system whose reachable set we know from a previous example. The plots show not only the case with uncertain parameters, but also the one without uncertain parameters. 

The MATLAB code that implements the simulation and reachability analysis of the nonlinear example with uncertain parameters is (the function is modified from the original file to better fit in this manual; line numbers after the first line jump due to the removed function description):
{\small
\input{./MATLABcode/example_nonlinearParam_reach_01_tank}}

The reachable set and the simulation are plotted in Fig. \ref{fig:example_nonlinearParam_reach_01_tank} for a time horizon of $t_f = 400$.
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinearParam_reach_01_tank_dim12.eps} \hspace{-0.2cm}
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinearParam_reach_01_tank_dim34.eps} \hspace{-0.2cm}
    \includegraphics[width=0.33\columnwidth]{./figures/examples/example_nonlinearParam_reach_01_tank_dim56.eps}
    \caption{Illustration of the reachable set of the linear example. The gray region shows the reachable set with uncertain parameters, while the white area shows the reachable set without uncertain parameters. Another white box shows the initial set and the black lines show simulated trajectories.}
    \label{fig:example_nonlinearParam_reach_01_tank}		
\end{figure}


\subsubsection{Discrete-time Nonlinear Systems}

We demonstrate the calculation of the reachable set for a time-discrete system with the example of a stirred tank reactor model. The original continuous time system model is given in \cite{Bravo2006}. Using the trapezoidal rule for time discretization, we obtained the following nonlinear discrete time system:
\begin{equation}
 \begin{split}
  & C_A(k+1) = \frac{1-\frac{q \tau}{2V} - k_0 \cdot \tau \cdot \exp{\left(-\frac{E}{R \cdot T(k)}\right)} \cdot C_A(k) + \frac{q}{V} \cdot C_{Af} \cdot \tau}{1 + \frac{q\tau}{2V} + w_1(k) \cdot \tau} \\
  & ~~ \\
  & T(k+1) = \frac{T(k) \cdot \left( 1 - \frac{\tau}{2} - \frac{\tau \cdot U A}{2V \cdot \rho \cdot C_p} \right) + \tau \cdot \left( T_f \cdot \frac{q}{V} + \frac{U A \cdot u(C_A(k),T(k))}{V \cdot \rho \cdot C_p} \right)}{1 + \frac{\tau \cdot q}{2V} + \frac{\tau \cdot U A}{2V \cdot \rho \cdot C_p}}\\
  & ~~ \\
  & ~~~~~~~~~~~~ - \frac{ C_A(k) \cdot \frac{\Delta H \cdot k_0 \cdot \tau}{\rho \cdot C_p} \cdot \exp{\left( - \frac{E}{R \cdot T(k)} \right)}}{1 + \frac{\tau \cdot q}{2V} + \frac{\tau \cdot U A}{2V \cdot \rho \cdot C_p}} + \tau \cdot w_2(k) ~~,
 \end{split}
\end{equation}
where $u(C_A(k),T(k)) = -3 \cdot C_A(k) -6.9 \cdot T(k)$ is the linear control law, $w_1(k) \in [-0.1,0.1]$ and $w_2(k) \in [-2,2]$ are bounded disturbances, and $\tau$ is the time step size. The values for the model parameters are given in \cite{Bravo2006}. The MATLAB code that implements the simulation and reachability analysis for the nonlinear discrete time model is shown below:

{\small
\input{./MATLABcode/example_nonlinearDT_reach_cstrDisc}}

The reachable set and the simulation are displayed in Fig. \ref{fig:exampleNonlinearSysDT} for a time horizon of $t_f = 0.15$ min. 
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.7\columnwidth]{./figures/examples/example_nonlinearDT_reach_cstrDisc.eps} 
    \caption{Illustration of the reachable set of the nonlinear discrete-time example. The black dots show the simulated points.}
    \label{fig:exampleNonlinearSysDT}		
\end{figure}

\subsubsection{Nonlinear Differential-Algebraic Systems}

CORA is also capable of computing reachable sets for semi-explicit, index-1 differential-algebraic equations. Although many index-1 differential-algebraic equations can be transformed into an ordinary differential equation, this is not always possible. For instance, power systems cannot be simplified due to Kirchhoff's law which constraints the currents of a node to sum up to zero. The capabilities of computing reachable sets are demonstrated for a small power system consisting of three buses. More complicated examples can be found in \cite{Althoff2012c,Althoff2014a,Althoff2014c}. 

The MATLAB code that implements the simulation and reachability analysis of the nonlinear example with uncertain parameters is (the function is modified from the original file to better fit in this manual; line numbers after the first line jump due to the removed function description):
{\small
\input{./MATLABcode/example_nonlinearDA_reach_01_powerSystem_3bus}}

The reachable set and the simulation are plotted in Fig. \ref{fig:example_nonlinearDA_reach_01_powerSystem_3bus} for a time horizon of $t_f = 5$.
\begin{figure}[htb]
  \centering	
    \footnotesize								 
    \includegraphics[width=0.4\columnwidth]{./figures/examples/example_nonlinearDA_reach_01_powerSystem_3bus_dim12.eps} 
    \caption{Illustration of the reachable set of nonlinear differential-algebraic example. The white box shows the initial set and the black lines show simulated trajectories.}
    \label{fig:example_nonlinearDA_reach_01_powerSystem_3bus}		
\end{figure}

\subsection{Hybrid Dynamics}

As already described in Sec.~\ref{sec:hybridDynamics}, CORA can compute reachable sets of mixed discrete/continuous or so-called hybrid systems. The difficulty in computing reachable sets of hybrid systems is the intersection of reachable sets with guard sets and the subsequent enclosure by the used set representation. Two major methods are demonstrated by the bouncing ball example and a powertrain example: geometric-based guard intersection for the bouncing ball example and mapping-based guard intersection for the powertrain example. The geometric-based approach is the dominant method in the literature (see e.g., \cite{Girard2008,Frehse2008,Frehse2011,Ramdani2011b,Frehse2012,Chen2015b,Althoff2009b}), but the mapping-based approach has shown great scalability for some examples \cite{Althoff2012a}. Determining advantages and disadvantages of both methods require further research.

\subsubsection{Bouncing Ball Example} \label{sec:bouncingBallExample}


We demonstrate the syntax of CORA for the well-known bouncing ball example, see e.g., \cite[Section 2.2.3]{Schaft2000}. 
Given is a ball in Fig. \ref{fig:bouncingBall} with dynamics $\ddot{s} = -g$, where $s$ is the vertical position and $g$ is the gravity constant. After impact with the ground at $s=0$, the velocity changes to $v' = -\alpha v$ ($v=\dot{s}$) with $\alpha \in [0,1]$. 
The corresponding hybrid automaton can be formalized according to Sec. \ref{sec:hybridDynamics} as

\begin{minipage}[l]{0.3\columnwidth}
\begin{center}
	  \psfrag{a}[c][c]{$s_0$}
	  \psfrag{b}[c][c]{$v_0$}
	  \psfrag{c}[c][c]{$g$} 
	  \includegraphics[width=0.8\columnwidth]{./figures/bouncingBall.eps}
	  \captionof{figure}{Bouncing ball.} \label{fig:bouncingBall}
	\end{center} 
\end{minipage}
\begin{minipage}[l]{0.7\columnwidth}
 \begin{equation*}
  \begin{array}{ll}
   \mathcal{V} &= \{v_1\} \\
   \mathcal{X} &= \mathbb{R}^+ \times \mathbb{R} \quad \text{(ball is above ground)} \\
   \mathcal{U} = \mathcal{Y}_c &= \{\} \\
   \mathtt{T} &= \{ (z_1, z_1) \} \\
   \mathtt{inv}(z_1) &= \{[x_1, x_2]^T | x_1 \in \mathbb{R}^+_0, x_2 \in \mathbb{R} \} \\
   \mathtt{g}\big( (z_1, z_1) \big) &= \{[x_1, x_2]^T | x_1 = 0, x_2 \in \mathbb{R}_0^- \} \\
   \mathtt{h}\big( (z_1, z_1), x \big) &= \begin{bmatrix} x_1 \\ -\alpha \, x_2 \end{bmatrix} \\
   \mathtt{f}(z_1,x) &= \begin{bmatrix} x_2 \\ -g \end{bmatrix} 
  \end{array}
 \end{equation*}
\end{minipage}

The MATLAB code that implements the simulation and reachability analysis of the bouncing ball example is (the function is modified from the original file to better fit in this manual; line numbers after the first line jump due to the removed function description):

{\small
\input{./MATLABcode/example_hybrid_reach_01_bouncingBall.tex}}

The reachable set and the simulation are plotted in Fig. \ref{fig:bouncingBallResult} for a time horizon of $t_f = 1.7$.

\begin{figure}[htb]
  \centering	
  	\footnotesize
		\psfrag{a}[c][c]{$x_1$}									 
		\psfrag{b}[c][c]{$x_2$}						
		\psfrag{c}[c][c]{initial set}									 
		\psfrag{d}[l][c]{simulated trajectory}		
		\psfrag{e}[l][c]{reachable set}									 

		\includegraphics[width=0.7\columnwidth]{./figures/bouncingBall_singleSim_c.eps}
    \caption{Illustration of the reachable set of the bouncing ball. The black box shows the initial set and the black line shows the simulated trajectory.}
    \label{fig:bouncingBallResult}		
\end{figure}

\subsubsection{Bouncing Ball Example (Converted From SpaceEx)} \label{sec:bouncingBallExampleSpaceEx}

This example is identical to the bouncing ball example shown in Sec.~\ref{sec:bouncingBallExample}, except that we use a model that has been automatically converted from SpaceEx. The MATLAB code that implements the simulation and reachability analysis of the bouncing ball example is (the function is modified from the original file to better fit in this manual; line numbers after the first line jump due to the removed function description):

{\small
\input{./MATLABcode/example_hybrid_reach_01_bouncingBall_converted.tex}}

The reachable set and the simulation are identical to the model in Sec.~\ref{sec:bouncingBallExample} and can be found in Fig. \ref{fig:bouncingBallResult} for a time horizon of $t_f = 1.7$.

\subsubsection{Powertrain Example}

The powertrain example is taken out of \cite[Sec.~6]{Althoff2012a}, which models the powertrain of a car with backlash. To investigate the scalability of the approach, one can add further rotating masses, similarly to adding further tanks for the tank example. Since the code of the powertrain example is rather lengthy, we are not presenting it in the manual; the interested reader can look it up in the example folder of the CORA code. The reachable set and the simulation are plotted in Fig. \ref{fig:example_hybrid_reach_02_powerTrain} for a time horizon of $t_f = 2$.

\begin{figure}[htb]
  \centering	
  	\footnotesize
		\includegraphics[width=0.4\columnwidth]{./figures/examples/example_hybrid_reach_02_powerTrain_dim12_b.eps}
		\includegraphics[width=0.4\columnwidth]{./figures/examples/example_hybrid_reach_02_powerTrain_dim13_b.eps}
    \caption{Illustration of the reachable set of the bouncing ball. The black box shows the initial set and the black line shows the simulated trajectory.}
    \label{fig:example_hybrid_reach_02_powerTrain}		
\end{figure}
