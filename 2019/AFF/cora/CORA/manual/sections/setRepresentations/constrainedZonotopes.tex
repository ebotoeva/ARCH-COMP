An extension of zonotopes described in Sec. \ref{sec:zonotope} are constrained zonotopes, which are introduced in \cite{Scott2016}. A constrained zonotope is defined as a zonotope with additional equality constraints on the factors $\beta_i$:
\begin{equation} \label{eq:constrainedZonotope}
	\mathcal{Z}_c = \Big\{ c + G \beta \Big| \lVert \beta \rVert_{\infty} \leq 1, A \beta = b \Big\}, 
\end{equation}
where $c \in \mathbb{R}^n$ is the zonotope center, $G \in \mathbb{R}^{n \times p}$ is the zonotope generator matrix and $\beta \in \mathbb{R}^p$ is the vector of zonotope factors. The equality constraints are parametrized by the matrix $A \in \mathbb{R}^{q \times p}$ and the vector $b \in \mathbb{R}^q$. Constrained zonotopes are able to describe arbitrary polytopes, and are therefore a more general set representation than zonotopes. The main advantage compared to a polytope representation using inequality constraints (see Sec. \ref{sec:mptPolytopes}) is that constrained zonotopes inherit the excellent scaling properties of zonotopes for increasing state space dimensions, since constrained zonotopes are also based on a generator representation for sets.   

Constrained zonotopes are implemented in the class \texttt{conZonotope}, which supports the following methods:
\begin{itemize}
 \item \texttt{and} -- computes the intersection of a \texttt{conZonotope} object with other set representations. More details can be found in Sec. \ref{sec:conZono_and}.
 \item \texttt{boundDir} -- computes an upper and lower bound for the projection of a \texttt{conZonotope} onto a certain state space direction (vector).
 \item \texttt{conZonotope} -- constructor of the class.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{enclose} -- generates a \texttt{conZonotope} object that encloses two constrained zonotopes. More details can be found in Sec. \ref{sec:conZono_enclose}.
 \item \texttt{interval} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{isempty} -- returns 1 if a \texttt{conZonotope} object is empty and 0 otherwise.
 \item \texttt{mptPolytope} -- converts a \texttt{conZonotope} object into a \texttt{mptPolytope} object.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in \cite{Scott2016}.
 \item \texttt{plot} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plotFilled} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plotZono} -- plots a two-dimensional projection of the \texttt{conZonotope} object together with the corresponding zonotope.
 \item \texttt{plus} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:conZono_plus}.
 \item \texttt{project} -- returns a \texttt{conZonotope} object, which is the projection of the input argument onto the specified dimensions.
 \item \texttt{reduce} -- returns an over-approximating \texttt{conZonotope} object with fewer generators and/or constraints as detailed in Sec.~\ref{sec:conZono_reduce}.
 \item \texttt{rescale} -- prune the domain of the zonotope factors $\beta_i$ by adequate adaption of the zonotope generators. More details can be found in \cite{Scott2016}.
 \item \texttt{vertices} -- returns a \texttt{vertices} object including all vertices of the \texttt{conZonotope} object (Warning: high computational complexity).
 \item \texttt{zonotope} -- returns a \texttt{zonotope} object that over-approximates the \texttt{conZonotope} object.
\end{itemize}




\subsubsection{Method \texttt{and}} \label{sec:conZono_and}

Table \ref{tab:conZono_and} lists the classes that can be intersected with a \texttt{conZonotope} object. Please note that the order plays a role and that the \texttt{conZonotope} object has to be on the left side of the \texttt{'\&'} operator.

\begin{table}[h]
\caption{Classes that can be intersected with a \texttt{conZonotope} object.}
\begin{center}\label{tab:conZono_and}
\begin{tabular}{lll}
	\hline 
	\textbf{class} & \textbf{reference} & \textbf{literature} \\ \hline
	\texttt{conZonotope} & Sec. \ref{sec:conZonotope} & \cite[Proposition 1]{Scott2016} \\
	\texttt{zonotope} & Sec. \ref{sec:zonotope} & - \\
	\texttt{interval} & Sec. \ref{sec:interval} & - \\
	\texttt{mptPolytope} & Sec. \ref{sec:mptPolytopes} & - \\
	\texttt{halfspace} & - & -\\ 
	\texttt{constrainedHyperplane} & - & -\\ \hline
\end{tabular}
\end{center}
\end{table}


\subsubsection{Method \texttt{enclose}} \label{sec:conZono_enclose}

The method \texttt{enclose} can only be applied if one constrained zonotope $\mathcal{Z}_{c,2}$ represents a linear transformation of the other constrained zonotope $\mathcal{Z}_{c,1}$:
\begin{equation} \label{eq:constrainedZonotopeEnclose}
	\mathcal{Z}_{c,2} = T \cdot \mathcal{Z}_{c,1} + t,
\end{equation}
where $T \in \mathbb{R}^{m \times n}$ is a transformation matrix and $n$ is the dimension of the state space. Table \ref{tab:conZono_enclose} lists the classes that are valid values for the variable $t$ in \eqref{eq:constrainedZonotopeEnclose}. The reason for this requirement is that with the restriction implied by \eqref{eq:constrainedZonotopeEnclose} it is possible to calculate a very tight enclosure of the \texttt{conZonotope} objects $\mathcal{Z}_{c,1}$ and $\mathcal{Z}_{c,2}$. In addition, the main application of the \texttt{enclose} function in CORA is the calculation of the convex hull during reachability analysis, where the restriction formulated in \eqref{eq:constrainedZonotopeEnclose} is always fulfilled. 

\begin{table}[h]
\caption{Classes that represent valid values for the vector $t$ in \eqref{eq:constrainedZonotopeEnclose}.}
\begin{center}\label{tab:conZono_enclose}
\begin{tabular}{lll}
	\hline 
	\textbf{class} & \textbf{reference} & \textbf{literature} \\ \hline
	MATLAB vector & - & - \\
	\texttt{zonotope} & Sec. \ref{sec:zonotope} & - \\
	\texttt{interval} & Sec. \ref{sec:interval} & - \\ \hline
\end{tabular}
\end{center}
\end{table}


\subsubsection{Method \texttt{plus}} \label{sec:conZono_plus}

Table \ref{tab:conZono_plus} lists the classes that can be added to a \texttt{conZonotope} object. Unlike with intersection, the \texttt{conZonotope} object can be on both sides of the \texttt{'+'} operator.


\begin{table}[h]
\caption{Classes that can be added to a \texttt{conZonotope} object.}
\begin{center}\label{tab:conZono_plus}
\begin{tabular}{lll}
	\hline 
	\textbf{class} & \textbf{reference} & \textbf{literature} \\ \hline
	MATLAB vector & - & - \\
	\texttt{conZonotope} & Sec. \ref{sec:conZonotope} & \cite[Proposition 1]{Scott2016} \\
	\texttt{zonotope} & Sec. \ref{sec:zonotope} & - \\
	\texttt{interval} & Sec. \ref{sec:interval} & - \\ \hline
\end{tabular}
\end{center}
\end{table}



\subsubsection{Method \texttt{reduce}} \label{sec:conZono_reduce}

One parameter to describe the complexity of a constrained zonotope is the \textit{degrees-of-freedom order} $o_c = (p-q)/n$, where $p$ represents the number of generators, $q$ is the number of constraints and $n$ is the state space dimension. The method \texttt{reduce} implements the two options reduction of the number of constraints $q$ \cite[Section 4.2]{Scott2016}, and reduction of the \textit{degrees-of-freedom order} $o_c$ \cite[Section 4.3]{Scott2016}.    



\subsubsection{Constrained Zonotope Example} \label{sec:conZonotopeExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/example_conZonotope.tex}}

This produces the workspace output
\begin{verbatim}
id: 0
dimension: 2
c: 
     0
     0

g_i: 
     1     0     1
     1     2    -1

A: 
    -2     1    -1

b: 
     2
\end{verbatim}

The plot generated in line 9 is shown in Fig. \ref{fig:conZonoExample_1}. Fig. \ref{fig:conZonoExample_2} displays a visualization of the constraints for the \texttt{conZonotope} object that is shown in Fig. \ref{fig:conZonoExample_1}. 

\begin{figure}[h!tb]
\begin{minipage}{0.45\columnwidth}
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=\columnwidth]{./figures/conZonotope.eps}
  \caption{Zonotope (blue) and the corresponding constrained zonotope (red) generated in the constrained zonotope example in Sec. \ref{sec:conZonotopeExample}}
  \label{fig:conZonoExample_1}
\end{minipage}
\hspace{0.08\columnwidth}
\begin{minipage}{0.45\columnwidth}
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=\columnwidth]{./figures/conZonotopesConstraints.eps}
  \caption{Visualization of the constraints for the \texttt{conZonotope} object generated in the constrained zonotope example in Sec. \ref{sec:conZonotopeExample}.}
  \label{fig:conZonoExample_2}
\end{minipage}
\end{figure}

