% This file was automatically created from the m-file 
% "m2tex.m" written by USL. 
% The fontencoding in this file is UTF-8. 
%  
% You will need to include the following two packages in 
% your LaTeX-Main-File. 
%  
% \usepackage{color} 
% \usepackage{fancyvrb} 
%  
% It is advised to use the following option for Inputenc 
% \usepackage[utf8]{inputenc} 
%  
  
% definition of matlab colors: 
\definecolor{mblue}{rgb}{0,0,1} 
\definecolor{mgreen}{rgb}{0.13333,0.5451,0.13333} 
\definecolor{mred}{rgb}{0.62745,0.12549,0.94118} 
\definecolor{mgrey}{rgb}{0.5,0.5,0.5} 
\definecolor{mdarkgrey}{rgb}{0.25,0.25,0.25} 
  
\DefineShortVerb[fontfamily=courier,fontseries=m]{\$} 
\DefineShortVerb[fontfamily=courier,fontseries=b]{\#} 
  
\noindent                                                                                                                                        
 \hspace*{-1.6em}{\scriptsize 1}$  $\color{mblue}$function$\color{black}$ example_nonlinearParam_reach_01_tank()$\\
 \hspace*{-2em}{\scriptsize 37}$  $\\
 \hspace*{-2em}{\scriptsize 38}$  dim=6;$\\
 \hspace*{-2em}{\scriptsize 39}$  $\\
 \hspace*{-2em}{\scriptsize 40}$  $\color{mgreen}$%set options --------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 41}$  options.tStart=0; $\color{mgreen}$%start time$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 42}$  options.tFinal=400; $\color{mgreen}$%final time$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 43}$  options.x0=[2; $\color{mred}$4; 4; 2; 10; 4]; $\color{black}$$\color{mgreen}$%initial state for simulation$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 44}$  options.R0=zonotope([options.x0,0.2*eye(dim)]); $\color{mgreen}$%initial set$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 45}$  options.timeStep=4;$\\
 \hspace*{-2em}{\scriptsize 46}$  options.taylorTerms=4; $\color{mgreen}$%number of taylor terms for reachable sets$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 47}$  options.intermediateOrder = options.taylorTerms;$\\
 \hspace*{-2em}{\scriptsize 48}$  options.zonotopeOrder=10; $\color{mgreen}$%zonotope order$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 49}$  options.reductionTechnique=$\color{mred}$'girard'$\color{black}$;$\\
 \hspace*{-2em}{\scriptsize 50}$  options.maxError = 1*ones(dim,1);$\\
 \hspace*{-2em}{\scriptsize 51}$  options.reductionInterval=1e3;$\\
 \hspace*{-2em}{\scriptsize 52}$  options.tensorOrder = 1;$\\
 \hspace*{-2em}{\scriptsize 53}$  $\\
 \hspace*{-2em}{\scriptsize 54}$  options.advancedLinErrorComp = 0;$\\
 \hspace*{-2em}{\scriptsize 55}$  $\\
 \hspace*{-2em}{\scriptsize 56}$  options.u=0; $\color{mgreen}$%input for simulation$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 57}$  options.U=zonotope([0,0.005]); $\color{mgreen}$%input for reachability analysis$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 58}$  options.uTrans=0; $\color{mgreen}$%has to be zero for nonlinear systems!!$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 59}$  $\\
 \hspace*{-2em}{\scriptsize 60}$  options.p=0.015; $\color{mgreen}$%parameter values for simulation$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 61}$  options.paramInt=interval(0.0148,0.015); $\color{mgreen}$%parameter intervals $\color{black}$$\\
 \hspace*{-2em}{\scriptsize 62}$  $\color{mgreen}$%--------------------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 63}$  $\\
 \hspace*{-2em}{\scriptsize 64}$  $\color{mgreen}$%--------------------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 65}$  $\\
 \hspace*{-2em}{\scriptsize 66}$  $\color{mgreen}$%specify continuous dynamics with and without uncertain parameters---------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 67}$  tankParam = nonlinParamSys(6,1,1,@tank6paramEq,options.maxError,options); $\color{mgreen}$$\\
 \hspace*{-2em}{\scriptsize 68}$  tank = nonlinearSys(6,1,@tank6Eq,options); $\color{mgreen}$$\\
 \hspace*{-2em}{\scriptsize 69}$  $\color{mgreen}$%--------------------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 70}$          $\\
 \hspace*{-2em}{\scriptsize 71}$  $\color{mgreen}$%compute reachable set of tank system with and without uncertain parameters$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 72}$  tic$\\
 \hspace*{-2em}{\scriptsize 73}$  RcontParam = reach(tankParam,options); $\color{mgreen}$%with uncertain parameters$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 74}$  tComp = toc;$\\
 \hspace*{-2em}{\scriptsize 75}$  disp([$\color{mred}$'time of reachable set with uncertain parameters: '$\color{black}$,num2str(tComp)]);$\\
 \hspace*{-2em}{\scriptsize 76}$  tic$\\
 \hspace*{-2em}{\scriptsize 77}$  RcontNoParam = reach(tank, options); $\color{mgreen}$%without uncertain parameters$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 78}$  tComp = toc;$\\
 \hspace*{-2em}{\scriptsize 79}$  disp([$\color{mred}$'time of reachable set without uncertain parameters: '$\color{black}$,num2str(tComp)]);$\\
 \hspace*{-2em}{\scriptsize 80}$  $\\
 \hspace*{-2em}{\scriptsize 81}$  $\color{mgreen}$%create random simulations; RRTs would provide better results, but are$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 82}$  $\color{mgreen}$%computationally more demanding$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 83}$  runs = 60;$\\
 \hspace*{-2em}{\scriptsize 84}$  fracV = 0.5;$\\
 \hspace*{-2em}{\scriptsize 85}$  fracI = 0.5;$\\
 \hspace*{-2em}{\scriptsize 86}$  changes = 6;$\\
 \hspace*{-2em}{\scriptsize 87}$  simRes = simulate_random(tank, options, runs, fracV, fracI, changes);$\\
 \hspace*{-2em}{\scriptsize 88}$  $\\
 \hspace*{-2em}{\scriptsize 89}$  $\\
 \hspace*{-2em}{\scriptsize 90}$  $\color{mgreen}$%plot results--------------------------------------------------------------$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 91}$  plotOrder = 8;$\\
 \hspace*{-2em}{\scriptsize 92}$  $\color{mblue}$for$\color{black}$ plotRun=1:3$\\
 \hspace*{-2em}{\scriptsize 93}$      $\color{mgreen}$% plot different projections$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 94}$      $\color{mblue}$if$\color{black}$ plotRun==1$\\
 \hspace*{-2em}{\scriptsize 95}$          dims=[1 $\color{mred}$2];$\color{black}$$\\
 \hspace*{-2em}{\scriptsize 96}$      $\color{mblue}$elseif$\color{black}$ plotRun==2$\\
 \hspace*{-2em}{\scriptsize 97}$          dims=[3 $\color{mred}$4];    $\color{black}$$\\
 \hspace*{-2em}{\scriptsize 98}$      $\color{mblue}$elseif$\color{black}$ plotRun==3$\\
 \hspace*{-2em}{\scriptsize 99}$          dims=[5 $\color{mred}$6]; $\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 100}$      $\color{mblue}$end$\color{black}$ $\\
 \hspace*{-2.4em}{\scriptsize 101}$  $\\
 \hspace*{-2.4em}{\scriptsize 102}$      figure;$\\
 \hspace*{-2.4em}{\scriptsize 103}$      hold $\color{mred}$on$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 104}$  $\\
 \hspace*{-2.4em}{\scriptsize 105}$      $\color{mgreen}$%plot reachable sets of zonotope; uncertain parameters$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 106}$      $\color{mblue}$for$\color{black}$ i=1:length(RcontParam)$\\
 \hspace*{-2.4em}{\scriptsize 107}$          $\color{mblue}$for$\color{black}$ j=1:length(RcontParam{i})$\\
 \hspace*{-2.4em}{\scriptsize 108}$              Zproj = reduce(RcontParam{i}{j},$\color{mred}$'girard'$\color{black}$,plotOrder);$\\
 \hspace*{-2.4em}{\scriptsize 109}$              plotFilled(Zproj,dims,[.675 .675 .675],$\color{mred}$'EdgeColor'$\color{black}$,$\color{mred}$'none'$\color{black}$);$\\
 \hspace*{-2.4em}{\scriptsize 110}$          $\color{mblue}$end$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 111}$      $\color{mblue}$end$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 112}$      $\\
 \hspace*{-2.4em}{\scriptsize 113}$      $\color{mgreen}$%plot reachable sets of zonotope; without uncertain parameters$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 114}$      $\color{mblue}$for$\color{black}$ i=1:length(RcontNoParam)$\\
 \hspace*{-2.4em}{\scriptsize 115}$          $\color{mblue}$for$\color{black}$ j=1:length(RcontNoParam{i})$\\
 \hspace*{-2.4em}{\scriptsize 116}$              Zproj = reduce(RcontNoParam{i}{j},$\color{mred}$'girard'$\color{black}$,plotOrder);$\\
 \hspace*{-2.4em}{\scriptsize 117}$              plotFilled(Zproj,dims,$\color{mred}$'w'$\color{black}$,$\color{mred}$'EdgeColor'$\color{black}$,$\color{mred}$'k'$\color{black}$);$\\
 \hspace*{-2.4em}{\scriptsize 118}$          $\color{mblue}$end$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 119}$      $\color{mblue}$end$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 120}$      $\\
 \hspace*{-2.4em}{\scriptsize 121}$      $\color{mgreen}$%plot initial set$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 122}$      plotFilled(options.R0,dims,$\color{mred}$'w'$\color{black}$,$\color{mred}$'EdgeColor'$\color{black}$,$\color{mred}$'k'$\color{black}$);$\\
 \hspace*{-2.4em}{\scriptsize 123}$      $\\
 \hspace*{-2.4em}{\scriptsize 124}$    $\\
 \hspace*{-2.4em}{\scriptsize 125}$      $\color{mgreen}$%plot simulation results      $\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 126}$      $\color{mblue}$for$\color{black}$ i=1:length(simRes.x)$\\
 \hspace*{-2.4em}{\scriptsize 127}$          plot(simRes.x{i}(:,dims(1)),simRes.x{i}(:,dims(2)),$\color{mred}$'k'$\color{black}$);$\\
 \hspace*{-2.4em}{\scriptsize 128}$      $\color{mblue}$end$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 129}$  $\\
 \hspace*{-2.4em}{\scriptsize 130}$      $\color{mgreen}$%label plot$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 131}$      xlabel([$\color{mred}$'x_{'$\color{black}$,num2str(dims(1)),$\color{mred}$'}'$\color{black}$]);$\\
 \hspace*{-2.4em}{\scriptsize 132}$      ylabel([$\color{mred}$'x_{'$\color{black}$,num2str(dims(2)),$\color{mred}$'}'$\color{black}$]);$\\
 \hspace*{-2.4em}{\scriptsize 133}$  $\color{mblue}$end$\color{black}$$\\
 \hspace*{-2.4em}{\scriptsize 134}$  $\color{mgreen}$%--------------------------------------------------------------------------$\color{black}$$\\
  
\UndefineShortVerb{\$} 
\UndefineShortVerb{\#}