function Z = encloseZonotopes(Zall,D,options)
% encloseZonotopes - encloses a set of zonotopes with a zonotope
%
% Syntax:  
%    [Z] = encloseZonotopes(Zall)
%
% Inputs:
%    Zall - cell array of zonotopes
%
% Outputs:
%    Z - enclosing zonotope
%
% Example: 
%
% Other m-files required: not specified
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Niklas Kochdumper
% Written:      16-May-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % initialization
    Zset = cell(length(D),1);
    
    % select function to calculate the interval over-approximation
    if strcmp(options.guardIntersect,'conZonotopeFast')   % fast algorithm
        
        % determine if the intersecting set consists of multiple parallel sets
        if ~iscell(Zall{1})
           parSets = isfield(Zall{1},'intersection'); 
        else
           parSets = isfield(Zall{1}{1},'intersection');
        end
        
        if parSets 
            func = @(x,y) intervalParSets(x,y);         % parallel sets
        else
            func = @(x,y) interval(y'*x);               % no parallel sets
        end
    else
        func = @(x,y) interval(y'*x);                      % slow algorithm
    end
    
    % loop over all othogonal basis
    for k = 1:length(D)

        infi = inf * ones(size(D{k},1),1);
        sup = -inf * ones(size(D{k},1),1);

        % loop over all sets
        for i = 1:length(Zall)
            
           if ~iscell(Zall{i})      % sets are not split

               % interval over-approximation in transformed space
               temp = func(Zall{i},D{k});

               % update bounds
               if ~isempty(temp)
                   infi = min(infi,infimum(temp));
                   sup = max(sup,supremum(temp));
               end
               
           else                     % sets are split
               
               for j = 1:length(Zall{1})
                   
                   % interval over-approximation in transformed space
                   temp = func(Zall{i}{j},D{k});
                   
                   % update bounds
                   if ~isempty(temp)
                       infi = min(infi,infimum(temp));
                       sup = max(sup,supremum(temp));
                   end
               end
           end
        end

        % transform bounding box back to original space
        temp = interval(infi,sup);
        Zset{k} = D{k}*zonotope(temp);  
    end
    
    % construct the resulting zonotopeBundle object
    if length(Zset) == 1
        Z = Zset{1}; 
    else
        Z = zonotopeBundle(Zset);
    end
end



% Auxiliary Functions -----------------------------------------------------

function res = intervalParSets(R,D)
% calcualte an over-approximating interval for a set which is indirectly 
% given by the intersection of multiple parallel sets

    infi = -inf * ones(size(D,1),1);
    sup = inf * ones(size(D,1),1);

    for i = 1:length(R.intersection)
        
        % interval over-approximation in transformed space
        int = interval(D'*R.intersection{i});
        
        % update bounds (real set = intersection of parallel sets)
        infi = max(infi,infimum(int));
        sup = min(sup,supremum(int));
    end
    
    res = interval(infi,sup);
end

%------------- END OF CODE --------------