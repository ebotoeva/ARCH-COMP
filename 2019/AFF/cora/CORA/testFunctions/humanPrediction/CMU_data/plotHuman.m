function plotHuman(posVector)
% plotHuman - plots a human in a given figure
% 
%
% Syntax:  
%    plotHuman(posVector)
%
% Inputs:
%    posVector - concatenated vector of 3D positions of markers 
%
% Outputs:
%    -
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      10-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% obtain incidence matrix
incMat = incidenceMatrix();

% set marker style
markerstyle = {'kx', 'kx', 'ko', 'ko',...
    'co', 'cx', 'co', 'cx', 'co', 'co', 'c+',...
    'go', 'gx', 'go', 'gx', 'go', 'go', 'g+', ...
    'r^', 'r^', 'r^', 'r^', 'rx', ...
    'yd', 'yd', 'yd', 'yd', ...
    'bx', 'bo', 'bx', 'bo', 'b+', 'bx', 'bo',...
    'mx', 'mo', 'mx', 'mo', 'm+', 'mx', 'mo',...
    };

% loop through all markers
for iMarker = 1:length(incMat)

    % extract trajectory
    markerPos = posVector(make3(iMarker));
    
    % plot marker
    plot3(markerPos(1),markerPos(2),markerPos(3),markerstyle{iMarker});

    % find neighbors of current marker
    neighbor = find(incMat(iMarker,:) == 1);

    % loop through all neighbors
    for i = 1:length(neighbor)
        
        % make sure that connecting lines are not plotted twice
        if neighbor(i) > iMarker 

            % extract trajectory
            neighborPos = posVector(make3(neighbor(i)));

            % plot connecting line
            X = [markerPos(1); neighborPos(1)];
            Y = [markerPos(2); neighborPos(2)];
            Z = [markerPos(3); neighborPos(3)];
            plot3(X,Y,Z,'k');
        end
    end
end




%------------- END OF CODE --------------