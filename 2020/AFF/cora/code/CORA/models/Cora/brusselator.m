function f = brusselator(x,u)

    f = [1 + x(1)^2*x(2) - 2.5*x(1);...
         1.5*x(1) - x(1)^2*x(2)];
     
end

