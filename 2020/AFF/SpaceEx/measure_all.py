import os
import string
import re
from subprocess import Popen, PIPE

# tools specified, can generalize loops below
mytool = 'spaceex'
tools = [mytool]
example = {mytool: []}

# examples organized with input/output file/string results for parsing runtime results
# Content of list:
# directory, instance, model
example[mytool].append(['Building', 'BLDC01-BDS01' , 'BLDC01'])
example[mytool].append(['Building', 'BLDF01-BDS01' , 'BLDF01'])
example[mytool].append(['Gear', 'GRBX01-MES01', 'GRBX01-MES01'])
example[mytool].append(['Gear', 'GRBX01-MES02', 'GRBX01-MES01'])
example[mytool].append(['Platoon', 'PLAD01-BND42', 'PLAD01-BND'])
example[mytool].append(['Platoon', 'PLAD01-BND30', 'PLAD01-BND'])
example[mytool].append(['Platoon', 'PLAN01-UNB50', 'PLAN01-UNB'])
example[mytool].append(['SpaceStation', 'ISSF01-ISS01', 'ISSF01'])
example[mytool].append(['SpaceStation', 'ISSF01-ISU01', 'ISSF01'])
example[mytool].append(['SpaceStation', 'ISSC01-ISS02', 'ISSC01'])
example[mytool].append(['SpaceStation', 'ISSC01-ISU02', 'ISSC01'])
example[mytool].append(['Rendezvous', 'SRNA01-SR02', 'SRNA01-SR0_'])
example[mytool].append(['Rendezvous', 'SRA01-SR02', 'SRA01-SR0_'])
example[mytool].append(['Rendezvous', 'SRU01-SR02', 'SRU01-SR0_'])
example[mytool].append(['Rendezvous', 'SRU02-SR02', 'SRU02-SR0_'])
#example[mytool].append(['Heat', 'HEAT3D', 'HEAT01', 'heat_5'])
#example[mytool].append(['Heat', 'HEAT3D', 'HEAT02', 'heat_10'])


# output result time list
output_time_l = {mytool: []}
safe = {mytool: []}

breakout_examples = 0
string_path_host = os.getcwd()

# iterate over all tools and all examples for each tool
for t in tools:
    #print("running ",t)
    for i in range(0, len(example[t])):
        #print("benchmark ",example[t][i][0])
        # set up command line call for each tool
        if t == mytool:
            breakout_examples = 0
            string_cmd = 'docker run -v ' + string_path_host + ':'+string_path_host +' -w '+string_path_host+' spaceex -g ' + example[t][i][0] + '/' + example[t][i][1] + '.cfg -m ' + example[t][i][0] + '/' + example[t][i][2] + '.xml' + ' -vl'
        else:
            print('ERROR: tool not indicated')
            break

        if breakout_examples == 0 or i == 0:
            # call system command via subprocess for pipe redirection
            p = Popen(string_cmd, stdout=PIPE,
                      stderr=PIPE, stdin=PIPE, shell=True)

            output = str(p.communicate())

        #print(string_cmd,"\n got:\n",output)

        # search string in stdout
        if t == mytool:
            fstr = 'Computing reachable states done after '
            sstr = 'not reachable'
            r_idx = output.rfind(fstr)
            s_idx = output.rfind(sstr)
            r_idx_time_start = int(r_idx) + len(fstr)
            r_idx_time_end = int(r_idx)+len(fstr)+6

            if s_idx >= 0:
                safe[mytool].append(1)
            else:
                safe[mytool].append(0)

        # r_idx non-negative if found
        if r_idx >= 0:
            output_time = output[r_idx_time_start: r_idx_time_end]
            # remove non-numeric characters (except .)
            output_time = re.sub("[^\d\.]", "", output_time)
            output_time_l[t].append(float(output_time))
        else:
            print(
                'WARNING: search string not found in output, check result order for consistency')


for i in range(len(example[mytool])):
    print(mytool + "," + example[mytool][i][1] + "," + "," + str(safe[mytool][i]) + "," + str(output_time_l[mytool][i]) + ",")
