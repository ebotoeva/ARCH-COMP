class AESVariableTracker2:
    def __init__(self, var_tracker=None, gmodel=None):

        if var_tracker is None:
            self.path_state_vars = []
            self.path_action_vars = []
            self.path_q_vars = []
        else:
            # copy the variables to the tracker
            gmodel.update()

            self.path_state_vars = [
                [gmodel.getVarByName(var.varName) for var in layer]
                for layer in var_tracker.path_state_vars
            ]

            self.path_action_vars = [
                [gmodel.getVarByName(var.varName) for var in layer]
                for layer in var_tracker.path_action_vars
            ]

            self.path_q_vars = [
                [gmodel.getVarByName(var.varName) for var in layer]
                for layer in var_tracker.path_q_vars
            ]

    def add_state_variables(self, state_vars):
        self.path_state_vars.append(state_vars)

    def add_action_variables(self, action_vars):
        self.path_action_vars.append(action_vars)

    def add_q_variables(self, q_vars):
        self.path_q_vars.append(q_vars)

    def get_witness_states(self):
        # return [
        #     [int(round(var.x)) for var in layer]
        #     for layer in self.path_state_vars
        # ]
        return [
            [var.x for var in layer]
            for layer in self.path_state_vars
        ]

    def get_witness_actions(self):
        # return [
        #     [int(round(var.x)) for var in layer]
        #     for layer in self.path_action_vars
        # ]
        return [
            [var.x for var in layer]
            for layer in self.path_action_vars
        ]


class AESVariableTracker:

    def __init__(self):
        self.path_state_vars = []
        self.path_action_vars = []
        self.path_q_vars = []

    def add_state_variables(self, state_vars):
        self.path_state_vars.append(state_vars)

    def add_action_variables(self, action_vars):
        self.path_action_vars.append(action_vars)

    def add_q_variables(self, q_vars):
        self.path_q_vars.append(q_vars)

    def get_witness_states(self, model):
        return [
            [model.getVarByName(var.varName).x for var in layer]
            for layer in self.path_state_vars
        ]

    def get_witness_actions(self, model):
        return [
            [model.getVarByName(var.varName).x for var in layer]
            for layer in self.path_action_vars
        ]

