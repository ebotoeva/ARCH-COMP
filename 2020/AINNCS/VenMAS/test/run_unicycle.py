#!/usr/bin/env python
import math

import datetime

import keras
import numpy as np
from common import mono_verify

from resources.unicycle.unicycle_agent import UnicycleAgent
from resources.unicycle.unicycle_env import UnicycleEnv

from src.network_parser.network_model import NetworkModel
from src.utils.formula import *
from src.verification.bounds.bounds import HyperRectangleBounds

def compute_trace():
    #(9.5, 9.5), (-4.5, -4.5), (2.1, 2.1), (1.5, 1.5)
    x1 = 9.5
    x2 = -4.5
    x3 = 2.11
    x4 = 1.5

    print("state", 0, ":", "[{}, {}, {}, {}]".format(x1, x2, x3, x4))

    dt = 0.2
    w = 0

    offset = 20
    scaling_factor = 1

    controller = keras.models.load_model("../resources/unicycle/models/controllerB_nnv.h5")
    steps = 10
    for i in range(steps):
        net_input = np.array([x1, x2, x3, x4]).reshape((1, 4))

        res = controller.predict(x=net_input, batch_size=1, verbose=0)
        [u1, u2] = res[0]

        u1 = (u1 - offset) * scaling_factor
        u2 = (u2 - offset) * scaling_factor

        print("action", i, ":", [u1, u2])
        next_x1 = x1 + x4 * math.cos(x3) * dt
        next_x2 = x2 + x4 * math.sin(x3) * dt
        next_x3 = x3 + u2 * dt
        next_x4 = x4 + u1 * dt + w

        x1 = next_x1
        x2 = next_x2
        x3 = next_x3
        x4 = next_x4

        print("state", i+1, ":", "[{}, {}, {}, {}]".format(x1, x2, x3, x4))


def main():


    agent, env = initialise_and_get_agent_and_env()
    #
    # Constraint specific variables of the initial state to one value by setting the upper
    # bounds equal to the lower bounds.

    # hardcoding values here. assuming variable order [x1, ... , x4]
    unzipped = zip(*[(9.5, 9.55), (-4.5, -4.45), (2.1, 2.11), (1.5, 1.51)])

    input_hyper_rectangle = HyperRectangleBounds(*unzipped)
    print(input_hyper_rectangle)

    # Leaf safety formula
    safe_x1_lower = VarConstConstraint(StateCoordinate(0), GT, -0.6)
    safe_x1_upper = VarConstConstraint(StateCoordinate(0), LT, 0.6)
    safe_x2_lower = VarConstConstraint(StateCoordinate(1), GT, -0.2)
    safe_x2_upper = VarConstConstraint(StateCoordinate(1), LT, 0.2)
    safe_x3_lower = VarConstConstraint(StateCoordinate(2), GT, -0.06)
    safe_x3_upper = VarConstConstraint(StateCoordinate(2), LT, 0.06)
    safe_x4_lower = VarConstConstraint(StateCoordinate(3), GT, -0.3)
    safe_x4_upper = VarConstConstraint(StateCoordinate(3), LT, 0.3)
    safe = NAryConjFormula([safe_x1_lower, safe_x1_upper, safe_x2_lower, safe_x2_upper, safe_x3_lower, safe_x3_upper, safe_x4_lower, safe_x4_upper])

    # we are required to check if controller ever reaches bad state dt of 0.2s
    steps = range(1,51)

    for num_steps in steps:
        print(num_steps, "steps")

        # Specify formula encoding safety spec
        formula = ANextFormula(num_steps, safe)

        # Run verification method.
        # Monotlithic encoding
        log_info = mono_verify(formula, input_hyper_rectangle, agent, env, timeout=1800)
        print("\n")

        with open("arch-comp.log", "a") as file:
            file.write(f"{datetime.datetime.now()}, Unicycle, {num_steps}, {log_info[0]:9.6f}, {log_info[1]}\n")

        if log_info[1] == "True":
            break


def initialise_and_get_agent_and_env():
    """
    Initialise agent and environment.
    :return: Initialised NeuralAgent and AccEnv objects.
    """

    x_cos_model = NetworkModel()
    x_cos_model.parse("../resources/unicycle/models/x_cos.h5")

    x_sin_model = NetworkModel()
    x_sin_model.parse("../resources/unicycle/models/x_sin.h5")

    controller_model = NetworkModel()
    controller_model.parse("../resources/unicycle/models/controllerB_nnv.h5")

    agent = UnicycleAgent(controller_model)
    env = UnicycleEnv(x_cos_model, x_sin_model)

    return agent, env


if __name__ == "__main__":
    main()
    # compute_trace()
