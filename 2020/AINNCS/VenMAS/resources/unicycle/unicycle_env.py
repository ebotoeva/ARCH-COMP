from src.actors.envs.environment import AbstractEnvironment
from src.network_parser.network_model import NetworkModel
from src.verification.bounds.bounds import HyperRectangleBounds


class UnicycleEnv(AbstractEnvironment):

    def __init__(self, x_cos_model, x_sin_model):
        """
        Environment for the Unicycle benchmark
        """

        assert isinstance(x_cos_model, NetworkModel)
        assert isinstance(x_sin_model, NetworkModel)

        # set the branching factor to 1
        super(UnicycleEnv, self).__init__(1)

        self.x_cos_model = x_cos_model
        self.x_sin_model = x_sin_model

        self.dt = 0.2
        self.w_lower = -0.0001
        self.w_upper = 0.0001

    def get_constraints_for_transition(self, i, constrs_manager, action_vars, input_state_vars):
        """
        :param i:
        :param constrs_manager:
        :param action_vars:
        :param input_state_vars: variables representing the current state
        :return:
        """

        constrs = []

        [x1, x2, x3, x4] = input_state_vars
        [u1, u2] = action_vars

        # Get and add the constraint for computing square.
        [x4__cos_x3], x_cos_constrs = \
            constrs_manager.get_network_constraints(self.x_cos_model.layers, [x3, x4]) #### guess this follows the order in which inputs appear in the net
        [x4__sin_x3], x_sin_constrs = \
            constrs_manager.get_network_constraints(self.x_sin_model.layers, [x3, x4])

        constrs.extend(x_cos_constrs)
        constrs.extend(x_sin_constrs)

        # Compute bounds for next state variables
        bounds = constrs_manager.get_variable_bounds([x1, x2, x3, x4, u1, u2])
        x_cos_bounds = constrs_manager.get_variable_bounds([x4__cos_x3])
        x_sin_bounds = constrs_manager.get_variable_bounds([x4__sin_x3])


        next_x1_lower, next_x1_upper = self.next_x1_bounds(bounds.get_lower(), bounds.get_upper(), x_cos_bounds.get_lower(), x_cos_bounds.get_upper())
        next_x2_lower, next_x2_upper = self.next_x2_bounds(bounds.get_lower(), bounds.get_upper(), x_sin_bounds.get_lower(), x_sin_bounds.get_upper())

        next_x3_lower, next_x3_upper = self.next_x3_bounds(bounds.get_lower(), bounds.get_upper())
        next_x4_lower, next_x4_upper = self.next_x4_bounds(bounds.get_lower(), bounds.get_upper())


        # Create next state variables with computed bounds
        next_state_vars = \
            constrs_manager.create_state_variables(4,
                                                   lbs=[next_x1_lower, next_x2_lower, next_x3_lower, next_x4_lower],
                                                   ubs=[next_x1_upper,next_x2_upper, next_x3_upper, next_x4_upper])
        constrs_manager.add_variable_bounds(next_state_vars,
                                            HyperRectangleBounds([next_x1_lower, next_x2_lower, next_x3_lower, next_x4_lower],
                                                                 [next_x1_upper, next_x2_upper, next_x3_upper, next_x4_upper]))


        # Add the constraints for computing next_x1 and others
        [next_x1, next_x2, next_x3, next_x4] = next_state_vars

        # next_x1 == x1 + x4 * cos(x3) *self.dt
        constrs.append(constrs_manager.get_linear_constraint([next_x1, x1, x4__cos_x3],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x2 == x2 + x4 * sin(x3) * self.dt
        constrs.append(constrs_manager.get_linear_constraint([next_x2, x2, x4__sin_x3],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x3 == x3  + u2 * self.dt
        constrs.append(constrs_manager.get_linear_constraint([next_x3, x3, u2],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x4 == x4 + u1 * self.dt + w
        constrs.append(constrs_manager.get_linear_constraint([next_x4, x4, u1],
                                                             [-1, 1, self.dt],
                                                             0))

        return next_state_vars, constrs


    def next_x1_bounds(self, input_lower, input_upper, cos_lower, cos_upper):
        """
        """
        # x1(t+1) = x1(t) + x4(t) * cos(x3(t)) * dt
        upper = input_upper[0] + self.dt * cos_upper[0]
        lower = input_lower[0] + self.dt * cos_lower[0]
        return lower, upper

    def next_x2_bounds(self, input_lower, input_upper, sin_lower, sin_upper):
        """
        """
        # x2(t+1) = x2(t) + x4(t) * sin(x3(t)) * dt
        upper = input_upper[1] + self.dt * sin_upper[0]
        lower = input_lower[1] + self.dt * sin_lower[0]
        return lower, upper

    def next_x3_bounds(self, input_lower, input_upper):
        """
        """
        # x3(t+1) = x3(t) + u2(t) * dt
        upper = input_upper[2] + input_upper[-1] * self.dt
        lower = input_lower[2] + input_lower[-1] * self.dt
        return lower, upper

    def next_x4_bounds(self, input_lower, input_upper):
        """
        """
        # x4(t+1) = x4(t) + u1(t) * dt + w
        upper = input_upper[3] + self.dt * input_upper[-2] + self.w_upper
        lower = input_lower[3] + self.dt * input_lower[-2] + self.w_lower
        return lower, upper
