#!/bin/bash

echo "Entering compile.sh"

make -B pds_stage1
make -B pds_stage2
make -B pds_stage3

make -B coupled_vdp-mu1-alex

make -B laub-loomis-tight
make -B laub-loomis-mid
make -B laub-loomis-large

make -B quadcopter

make -B lotka-volterra

make -B spacecraft

