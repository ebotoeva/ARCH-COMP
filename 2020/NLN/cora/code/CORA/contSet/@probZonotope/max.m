function [result] = max(pZ,m)
% max - Computes an overaproximation of the maximum on the m-sigma bound
%       according to Eq. (3) in [1]
%
% Syntax:  
%    [result] = max(pZ,m)
%
% Inputs:
%    pZ - probabilistic zonotope object
%    m - m of the m-sigma bound
%
% Outputs:
%    result - overapproximated maximum value
%
% References:
%    [1] M. Althoff et al. "Safety assessment for stochastic linear systems 
%        using enclosing hulls of probability density functions", ECC 2009
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: probZonotope

% Author:       Matthias Althoff
% Written:      22-August-2007
% Last update:  08-September-2009
% Last revision:---

%------------- BEGIN CODE --------------

%obtain covariance matrix
Sigma=sigma(pZ);

%get dimension
dim=length(pZ.g(:,1));

%compute maximum value
result=1/((2*pi)^(dim/2)*det(Sigma)^(1/2))*exp(-0.5*m^2);

%------------- END OF CODE --------------