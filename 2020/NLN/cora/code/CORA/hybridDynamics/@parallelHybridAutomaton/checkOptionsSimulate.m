function checkOptionsSimulate(obj,options)
% checkOptionsSimulate - checks if all necessary options
%   are there and have valid values
%
% Syntax:
%   checkOptionsSimulate(opt)
%
% Inputs:
%    obj     - parallelHybridAutomaton object
%    options - options for parallelHybridAutomaton simulation (1x1 struct)
%
% Outputs:
%    -
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger
% Written:      19-Feb-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

checkName = "checkOptionsSimulate";


% MANDATORY OPTIONS -------------------------------------------------------

check_metasim(options,obj,true);

check_R0(options, obj);
check_pHA_inputs(options, obj);
options = check_tStart_tFinal(obj, options, checkName);
check_startLoc_finalLoc(options, obj, 1);


% WARNINGS ----------------------------------------------------------------

% necessary fields 
validFields = getValidFields('parallelHybridAutomaton_sim');
printRedundancies(options, validFields);


end

%------------- END OF CODE --------------
